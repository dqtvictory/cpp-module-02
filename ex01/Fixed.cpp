#include <iostream>
#include <cmath>
#include "Fixed.hpp"

static int	floatToRawBits(const float f, const int fracBits)
{
	float	val = f;

	for (int i = 0; i < fracBits; i++)
		val *= 2.0f;
	return ((int)roundf(val));
}


Fixed::Fixed(void) : _value(0)
{
	std::cout << "Default constructor called\n";
}

Fixed::Fixed(const int i) : _value(i << _fracBits)
{
	std::cout << "Int constructor called\n";
}

Fixed::Fixed(const float f) : _value(floatToRawBits(f, _fracBits))
{
	std::cout << "Float constructor called\n";
}

Fixed::Fixed(const Fixed &fx)
{
	std::cout << "Copy constructor called\n";
	*this = fx;
}

Fixed	&Fixed::operator=(const Fixed &fx)
{
	std::cout << "Assignation operator called\n";
	setRawBits(fx._value);
	return (*this);
}

Fixed::~Fixed(void)
{
	std::cout << "Destructor called\n";
}

int	Fixed::getRawBits(void) const
{
	return (_value);
}

void	Fixed::setRawBits(int const raw)
{
	_value = raw;
}

float	Fixed::toFloat(void) const
{
	float	val = (float)_value;

	for (int i = 0; i < _fracBits; i++)
		val /= 2.0f;
	return (val);
}

int	Fixed::toInt(void) const
{
	return (_value >> _fracBits);
}
