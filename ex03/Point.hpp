#ifndef POINT_HPP
#define POINT_HPP

#include "Fixed.hpp"

class Point
{
private:
	Fixed const	_x;
	Fixed const	_y;
public:
	Point(void);
	Point(const float x, const float y);
	Point(const Point &pt);
	Point	&operator=(const Point &pt);
	~Point(void);

	bool	operator==(const Point &pt) const;
	bool	operator!=(const Point &pt) const;

	float	getX(void) const;
	float	getY(void) const;
};

#endif
