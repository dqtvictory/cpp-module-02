#include "Point.hpp"
#include <cfloat>

#define SLOPE_INF FLT_MAX

float	slope(const Point &p1, const Point &p2)
{
	if (p1.getX() == p2.getX())
		return (SLOPE_INF);
	return ((p2.getY() - p1.getY()) / (p2.getX() - p1.getX()));
}

static bool
sameSpace(const Point &l1, const Point &l2, const Point &p1, const Point &p2)
{
	// Line equation formed by two points (x1, y1) and (x2, y2)
	// is given by (y - y_) - m(x - x_) = 0 where m is the slope
	// and (x_, y_) can be either point
	bool	side1, side2;
	float	m = slope(l1, l2);

	// Check if slope is infinity, then p1 and p2 is on the same space
	// only if they are on the same side partitioned by the vertical line
	// formed by l1 and l2
	if (m == SLOPE_INF)
		return ((p1.getX() > l1.getX()) == (p2.getX() > l2.getX()));

	side1 = (((p1.getY() - l1.getY()) - m * (p1.getX() - l1.getX())) > 0);
	side2 = (((p2.getY() - l2.getY()) - m * (p2.getX() - l2.getX())) > 0);
	return (side1 == side2);
}

bool
bsp(Point const a, Point const b, Point const c, Point const point)
{
	// Check if point is identical to any of the vertices
	if (a == point || b == point || c == point)
		return (false);

	const Point	triangle[] = {a, b, c};
	int j, k;

	for (int i = 0; i < 3; i++)
	{
		j = (i + 1) % 3;
		k = (i + 2) % 3;

		// Check if point is on the line formed by two vertices of triangle
		if (slope(triangle[j], point) == slope(triangle[k], point))
			return (false);

		// Check if point and the other vertex are on the same binary space
		// formed by the line connecting two vertices of the triangle
		if (!sameSpace(triangle[j], triangle[k], triangle[i], point))
			return (false);
	}
	return (true);
}
