#include <iostream>
#include <cmath>
#include "Fixed.hpp"

////////	HELPER FUNCTIONS	////////

static int	floatToRawBits(const float f, const int fracBits)
{
	float	val = f;

	for (int i = 0; i < fracBits; i++)
		val *= 2.0f;
	return ((int)roundf(val));
}

////////	CON/DE-STRUCTORS	////////

Fixed::Fixed(void) : _value(0)
{}

Fixed::Fixed(const int i) : _value(i << _fracBits)
{}

Fixed::Fixed(const float f) : _value(floatToRawBits(f, _fracBits))
{}

Fixed::Fixed(const Fixed &fx)
{
	*this = fx;
}

Fixed	&Fixed::operator=(const Fixed &fx)
{
	setRawBits(fx._value);
	return (*this);
}

Fixed::~Fixed(void)
{}

////////	MEMBER FUNCTIONS	////////

int	Fixed::getRawBits(void) const
{
	return (_value);
}

void	Fixed::setRawBits(int const raw)
{
	_value = raw;
}

float	Fixed::toFloat(void) const
{
	float	val = (float)_value;

	for (int i = 0; i < _fracBits; i++)
		val /= 2.0f;
	return (val);
}

int	Fixed::toInt(void) const
{
	return (_value >> _fracBits);
}

////////	OPERATOR OVERLOADS	////////

bool	Fixed::operator>(const Fixed &fx) const
{
	return (_value > fx._value);
}

bool	Fixed::operator<(const Fixed &fx) const
{
	return (_value < fx._value);
}

bool	Fixed::operator>=(const Fixed &fx) const
{
	return (_value >= fx._value);
}

bool	Fixed::operator<=(const Fixed &fx) const
{
	return (_value <= fx._value);
}

bool	Fixed::operator==(const Fixed &fx) const
{
	return (_value == fx._value);
}

bool	Fixed::operator!=(const Fixed &fx) const
{
	return (_value != fx._value);
}


Fixed	Fixed::operator+(const Fixed &fx)
{
	Fixed	res;

	res.setRawBits(_value + fx._value);
	return (res);
}

Fixed	Fixed::operator-(const Fixed &fx)
{
	Fixed	res;

	res.setRawBits(_value - fx._value);
	return (res);
}

Fixed	Fixed::operator*(const Fixed &fx)
{
	Fixed	res(*this);

	res.setRawBits(_value * fx.toFloat());
	return (res);
}

Fixed	Fixed::operator/(const Fixed &fx)
{
	Fixed	res(*this);

	res.setRawBits(_value / fx.toFloat());
	return (res);
}


Fixed	&Fixed::operator++()
{
	_value++;
	return (*this);
}

Fixed	Fixed::operator++(int)
{
	Fixed	temp(*this);
	++(*this);
	return (temp);
}

Fixed	&Fixed::operator--()
{
	_value--;
	return (*this);
}

Fixed	Fixed::operator--(int)
{
	Fixed	temp(*this);
	--(*this);
	return (temp);
}

////////	STATIC MEMBER FUNCTIONS	////////
const Fixed	&Fixed::min(const Fixed &f1, const Fixed &f2)
{
	return (f1._value < f2._value ? f1 : f2);
}

const Fixed	&Fixed::max(const Fixed &f1, const Fixed &f2)
{
	return (f1._value > f2._value ? f1 : f2);
}
