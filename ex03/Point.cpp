#include "Point.hpp"

Point::Point(void) : _x(), _y()
{}

Point::Point(const float x, const float y) : _x(x), _y(y)
{}

Point::Point(const Point &pt) : _x(pt._x), _y(pt._y)
{}

Point	&Point::operator=(const Point &pt)
{
	return ((Point &)pt);
}

Point::~Point()
{}

bool	Point::operator==(const Point &pt) const
{
	return (_x == pt._x && _y == pt._y);
}

bool	Point::operator!=(const Point &pt) const
{
	return (!(*this == pt));
}


float	Point::getX(void) const
{
	return (_x.toFloat());
}
float	Point::getY(void) const
{
	return (_y.toFloat());
}
