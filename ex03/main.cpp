#include <iostream>
#include <cstdlib>
#include "Point.hpp"

float	slope(const Point &p1, const Point &p2);
bool	bsp(Point const a, Point const b, Point const c, Point const point);

std::ostream	&operator<<(std::ostream &os, const Fixed &fx)
{
	return (os << fx.toFloat());
}

std::ostream	&operator<<(std::ostream &os, const Point &pt)
{
	return (os << "<" << pt.getX() << ", " << pt.getY() << ">");
}

static bool	isTriangle(const Point &p1, const Point &p2, const Point &p3)
{
	// Check if any two points are identical
	if (p1 == p2 || p2 == p3 || p1 == p3)
		return (false);

	// Check if all three points are on the same line
	if (slope(p1, p2) == slope(p2, p3))
		return (false);
	return (true);
}

int	main(int ac, char **av)
{
	// Link to graphing calculator:
	// https://www.desmos.com/calculator
	
	if (ac != 9)
	{
		std::cout	<< "\033[91m"
					<< "Program must be launched and have exactly 8 arguments as following:\n"
					<< "\033[4m./bsp v1X v1Y v2X v2Y v3X v3Y ptX ptY\033[0m \033[91m\n"
					<< "where v1 v2 v3 are three vertices of a triangle and pt is a point.\033[0m"
					<< std::endl;
		return (1);
	}

	Point	v1(std::atof(av[1]), std::atof(av[2]));
	Point	v2(std::atof(av[3]), std::atof(av[4]));
	Point	v3(std::atof(av[5]), std::atof(av[6]));
	Point	pt(std::atof(av[7]), std::atof(av[8]));

	if (!isTriangle(v1, v2 ,v3))
	{
		std::cout << "Are you kidding me? This is not a triangle" << std::endl;
		return (1);
	}

	std::cout	<< "Triangle: " << v1 << " " << v2 << " " << v3 << "\n"
				<< "Point: " << pt << "\n";

	std::string	res = bsp(v1, v2, v3, pt) ? "INSIDE" : "OUTSIDE";
	std::cout << res << std::endl;
}
