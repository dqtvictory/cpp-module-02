#ifndef FIXED_HPP
#define FIXED_HPP

#include <iostream>

class Fixed
{

private:
	static const int	_fracBits = 8;
	int					_value;

public:
	Fixed(void);
	Fixed(const int i);
	Fixed(const float f);
	Fixed(const Fixed &fx);
	Fixed	&operator=(const Fixed &fx);
	~Fixed(void);

	int		getRawBits(void) const;
	void	setRawBits(int const raw);
	float	toFloat(void) const;
	int		toInt(void) const;

	bool	operator>(const Fixed &fx) const;
	bool	operator<(const Fixed &fx) const;
	bool	operator>=(const Fixed &fx) const;
	bool	operator<=(const Fixed &fx) const;
	bool	operator==(const Fixed &fx) const;
	bool	operator!=(const Fixed &fx) const;

	Fixed	operator+(const Fixed &fx);
	Fixed	operator-(const Fixed &fx);
	Fixed	operator*(const Fixed &fx);
	Fixed	operator/(const Fixed &fx);

	Fixed	&operator++();
	Fixed	operator++(int);
	Fixed	&operator--();
	Fixed	operator--(int);

	static const Fixed	&min(const Fixed &f1, const Fixed &f2);
	static const Fixed	&max(const Fixed &f1, const Fixed &f2);
};

#endif
