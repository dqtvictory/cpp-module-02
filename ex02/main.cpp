#include <iostream>
#include "Fixed.hpp"

std::ostream	&operator<<(std::ostream &os, const Fixed &fx)
{
	return (os << fx.toFloat());
}

int main( void )
{
	Fixed 		a;
	Fixed const	b(Fixed(5.05f) * Fixed(2));

	std::cout << "=== Stream and incre/decrement operations overloading ===\n";
	std::cout << a << "\n";
	std::cout << ++a << "\n";
	std::cout << a << "\n";
	std::cout << a++ << "\n";
	std::cout << a << "\n\n";

	std::cout << "=== Mathematics operations overloading ===\n";
	std::cout << "+ " << Fixed(5.05f) + Fixed(2) << "\n";
	std::cout << "- " << Fixed(5.05f) - Fixed(2) << "\n";
	std::cout << "/ " << Fixed(5.05f) / Fixed(2) << "\n";
	std::cout << "* " << b << "\n\n";

	std::cout << "=== Min/Max ===\n";
	std::cout << "max: " << Fixed::max(a, b) << "\n";
	std::cout << "min: " << Fixed::min(a, b) << "\n\n";

	std::cout << "=== Comparison operations overloading ===\n";
	std::cout << "==: " << (Fixed(5.0f) == Fixed(5)) << "\n";
	std::cout << "!=: " << (Fixed(5.0f) != Fixed(5)) << "\n";
	std::cout << "> : " << (Fixed(5.1f) > Fixed(5)) << "\n";
	std::cout << "< : " << (Fixed(5.1f) < Fixed(5)) << "\n";
	std::cout << ">=: " << (Fixed(5.0f) >= Fixed(5)) << "\n";
	std::cout << "<=: " << (Fixed(5.0f) <= Fixed(5)) << "\n";
}
