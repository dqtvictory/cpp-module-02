#include <iostream>
#include "Fixed.hpp"

Fixed::Fixed(void) : _value(0)
{
	std::cout << "Default constructor called\n";
}

Fixed::Fixed(const Fixed &fx)
{
	std::cout << "Copy constructor called\n";
	*this = fx;
}

Fixed	&Fixed::operator=(const Fixed &fx)
{
	std::cout << "Assignation operator called\n";
	setRawBits(fx.getRawBits());
	return (*this);
}

Fixed::~Fixed(void)
{
	std::cout << "Destructor called\n";
}

int	Fixed::getRawBits(void) const
{
	std::cout << "getRawBits member function called\n";
	return (_value);
}

void	Fixed::setRawBits(int const raw)
{
	_value = raw;
}
