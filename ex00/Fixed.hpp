#ifndef FIXED_HPP
#define FIXED_HPP

class Fixed
{

private:
	static const int	_fracBits = 8;
	int					_value;

public:
	Fixed(void);
	Fixed(const Fixed &f);
	Fixed	&operator=(const Fixed &fx);
	~Fixed(void);
	
	int		getRawBits(void) const;
	void	setRawBits(int const raw);

};

#endif
